# Docker in Docker with Molecule

Dockerfile to build a Docker in Docker image with molecule installed for testing ansible roles.
It's based off Alpine Stabel DiND and installs:

- Ansible Versions 2.8, 2.9, 2.10
- ansible-lint
- molecule[lint]
- molecule[docker]

You will need Docker set up and running on your system.

## Build

```
docker build . -t awollinux/dind-molecule:latest
```

If the build is successful, you'll be able to see your new image, for example:

```
[awol@gaming-amd ~]$ docker images
REPOSITORY                TAG                 IMAGE ID            CREATED              SIZE
awollinux/dind-molecule   latest              a6d478eb3c63        About a minute ago   1.09GB
docker                    stable-dind         71edd6fcc7ef        5 weeks ago          232MB
[awol@gaming-amd ~]$ 
```

## Run

```
docker run -it awollinux/dind-molecule /bin/sh
```
Please note bash isnt installed so make sure you call either sh or python
