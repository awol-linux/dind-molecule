#pull archlinux
FROM docker:stable-dind 

RUN apk add --no-cache \
    python3 python3-dev py3-pip py3-virtualenv gcc git curl build-base autoconf automake py3-cryptography linux-headers musl-dev libffi-dev openssl-dev openssh sudo

ENV VIRTUAL_ENV=/opt/ansible28
RUN python3 -m venv $VIRTUAL_ENV
ENV PATH="$VIRTUAL_ENV/bin:$PATH"

# Install dependencies:
RUN python3 -m pip install ansible==2.8 ansible-lint==4.3 molecule[docker] molecule[lint]

# create second enviroment
ENV VIRTUAL_ENV=/opt/ansible29
RUN python3 -m venv $VIRTUAL_ENV
ENV PATH="$VIRTUAL_ENV/bin:$PATH"

# Install dependencies:
RUN python3 -m pip install ansible==2.9 ansible-lint==4.3 molecule[docker] molecule[lint]

ENV VIRTUAL_ENV=/opt/ansible210
RUN python3 -m venv $VIRTUAL_ENV
ENV PATH="$VIRTUAL_ENV/bin:$PATH"

# Install dependencies:
RUN python3 -m pip install ansible==2.9 ansible-lint==4.3 molecule[docker] molecule[lint]
